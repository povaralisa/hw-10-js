"use strict";
const form = document.querySelector(".password-form");
const error = document.createElement("p");
error.textContent = `Нужно ввести одинаковые значения`;
error.style.color = "red";
btn.addEventListener("click", (e) => {
  e.preventDefault();
  let input = document.querySelectorAll("input");
  if (input[0].value && input[1].value) {
    if (input[0].value === input[1].value) {
      error.remove();
      alert("You are welcome");
    } else {
      document.querySelector("button").before(error);
    }
  }
});
let password = document.querySelectorAll(".icon-password");
password.forEach((elem) => {
  elem.addEventListener("click", (e) => {
    let input = e.target.parentNode.querySelector("input");
    if (input.getAttribute("type") === "password") {
      input.setAttribute("type", "text");
      e.target.classList.remove("fa-eye");
      e.target.classList.add("fa-eye-slash");
    } else {
      input.setAttribute("type", "password");
      e.target.classList.remove("fa-eye-slash");
      e.target.classList.add("fa-eye");
    }
  });
});
